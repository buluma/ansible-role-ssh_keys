# [ssh_keys](#ssh_keys)

Manage ssh public key authentication (public / private / authorized keys and known hosts) in Debian-like systems

|GitHub|GitLab|Quality|Downloads|Version|Issues|Pull Requests|
|------|------|-------|---------|-------|------|-------------|
|[![github](https://github.com/buluma/ansible-role-ssh_keys/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-ssh_keys/actions)|[![gitlab](https://gitlab.com/buluma/ansible-role-ssh_keys/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-ssh_keys)|[![quality](https://img.shields.io/ansible/quality/59113)](https://galaxy.ansible.com/buluma/ssh_keys)|[![downloads](https://img.shields.io/ansible/role/d/59113)](https://galaxy.ansible.com/buluma/ssh_keys)|[![Version](https://img.shields.io/github/release/buluma/ansible-role-ssh_keys.svg)](https://github.com/buluma/ansible-role-ssh_keys/releases/)|[![Issues](https://img.shields.io/github/issues/buluma/ansible-role-ssh_keys.svg)](https://github.com/buluma/ansible-role-ssh_keys/issues/)|[![PullRequests](https://img.shields.io/github/issues-pr-closed-raw/buluma/ansible-role-ssh_keys.svg)](https://github.com/buluma/ansible-role-ssh_keys/pulls/)|

## [Example Playbook](#example-playbook)

This example is taken from `molecule/default/converge.yml` and is tested on each push, pull request and release.
```yaml
---
- name: Converge
  hosts: all
  become: yes
  gather_facts: yes

  vars:
    ssh_keys_generate_keys:
      - path: ../../../files/ssh-keys/id_rsa
        comment: RSA key
    ssh_keys_private_keys:
      - owner: root
        src: ../../../files/ssh-keys/id_rsa
    ssh_keys_public_keys:
      - owner: root
        src: ../../../files/ssh-keys/id_rsa.pub
    ssh_keys_authorized_keys:
      - owner: root
        src: ../../../files/ssh-keys/id_rsa.pub
    ssh_keys_known_hosts:
      - hostname: github.com
        enctype: ssh-rsa
        fingerprint: 'AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ=='

  roles:
    - buluma.ssh_keys
```

The machine needs to be prepared. In CI this is done using `molecule/default/prepare.yml`:
```yaml
---
- name: prepare
  hosts: all
  become: yes
  gather_facts: no

  roles:
    - role: buluma.bootstrap
    - role: buluma.core_dependencies
```


## [Role Variables](#role-variables)

The default values for the variables are set in `defaults/main.yml`:
```yaml
# defaults file
---
ssh_keys_generate_keys: []
ssh_keys_generate_keys_become: false
ssh_keys_private_keys: []
ssh_keys_public_keys: []
ssh_keys_authorized_keys: []
ssh_keys_known_hosts: []
```

## [Requirements](#requirements)

- pip packages listed in [requirements.txt](https://github.com/buluma/ansible-role-ssh_keys/blob/main/requirements.txt).

## [Status of used roles](#status-of-requirements)

The following roles are used to prepare a system. You can prepare your system in another way.

| Requirement | GitHub | GitLab |
|-------------|--------|--------|
|[buluma.bootstrap](https://galaxy.ansible.com/buluma/bootstrap)|[![Build Status GitHub](https://github.com/buluma/ansible-role-bootstrap/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-bootstrap/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-bootstrap/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-bootstrap)|
|[buluma.core_dependencies](https://galaxy.ansible.com/buluma/core_dependencies)|[![Build Status GitHub](https://github.com/buluma/ansible-role-core_dependencies/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-core_dependencies/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-core_dependencies/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-core_dependencies)|

## [Context](#context)

This role is a part of many compatible roles. Have a look at [the documentation of these roles](https://buluma.github.io/) for further information.

Here is an overview of related roles:

![dependencies](https://raw.githubusercontent.com/buluma/ansible-role-ssh_keys/png/requirements.png "Dependencies")

## [Compatibility](#compatibility)

This role has been tested on these [container images](https://hub.docker.com/u/buluma):

|container|tags|
|---------|----|
|ubuntu|all|
|debian|all|

The minimum version of Ansible required is 2.10, tests have been done to:

- The previous version.
- The current version.
- The development version.



If you find issues, please register them in [GitHub](https://github.com/buluma/ansible-role-ssh_keys/issues)

## [Changelog](#changelog)

[Role History](https://github.com/buluma/ansible-role-ssh_keys/blob/master/CHANGELOG.md)

## [License](#license)

Apache-2.0

## [Author Information](#author-information)

[buluma](https://buluma.github.io/)
